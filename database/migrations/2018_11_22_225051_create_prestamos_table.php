<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrestamosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prestamos', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('estudiante_id');
            $table->unsignedInteger('empleado_id');
            $table->unsignedInteger('libro_id');
           

            $table->foreign('estudiante_id')
                ->references('id') 
                ->on('estudiantes');

            $table->foreign('empleado_id')
                ->references('id') 
                ->on('empleados');

            $table->foreign('libro_id')
                ->references('id') 
                ->on('libros');

            $table->date('fecha-prestamo');
            $table->date('fecha-devolucion');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prestamos');
    }
}
