@extends('master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
    
        <div class="col-md-8 text-center">
            <h1>Estudiantes Disponibles</h1>
        </div>
              
        <div class="col-md-8 mt-2">
            <div class="card">
                <div class="card-body">
                <div>
                    <p>Nombre: {{$estudiante->nombre}}</p>
                    <p>Apellido: {{$estudiante->apellido}}</p>
                    <p>Cedula: {{$estudiante->cedula}}</p>
                    <p>Carrera: {{$estudiante->carrera}}</p>
                    <p>Telefono: {{$estudiante->telefono}}</p>
                    <p>Correo Electronico: {{$estudiante->correo}}</p>
                </div>
                </div>
            </div>
        </div>
      
    </div>
</div>
@endsection
