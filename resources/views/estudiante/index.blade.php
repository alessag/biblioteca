@extends('master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @forelse ($estudiantes as $estudiante)
        @if ($loop->first)
        <div class="col-md-8 text-center">
            <h1>Estudiantes Actuales</h1>
        </div>
        @endif
        
        <div class="col-md-8 mt-2">
            <div class="card">
                <div class="card-header"> <a class="btn" href="{{ route('estudiante.show', $estudiante) }}"> {{$estudiante->title}} </a></div>
                <div class="card-body">
                <div>
                    <p>Nombre: {{$estudiante->nombre}}</p>
                    <p>Apellido: {{$estudiante->apellido}}</p>
                    <p>Cedula: {{$estudiante->cedula}}</p>
                    <p>Carrera: {{$estudiante->carrera}}</p>
                    <p>Telefono: {{$estudiante->telefono}}</p>
                    <p>Correo Electronico: {{$estudiante->correo}}</p>
                </div>
                </div>
            </div>
        </div>

         <div class=" d-inline-flex ">
            <a href="{{route('estudiante.edit', $estudiante)}}" class="btn btn-outline-success btn-md mr-2">Editar</a>
                <form action="{{route('estudiante.destroy', $estudiante)}}" method="POST">
                    @method('DELETE')
                    @csrf
                    <button type="submit" href="{{route('estudiante.edit', $estudiante)}}" class="btn btn-danger btn-md">Borrar</button>
                    </form>
        </div>


        @empty
        <div class="col-md-8 text-center">
            <h1>Aun no hay esudiantes</h1>
        </div>
        @endforelse

       

    </div>
</div>
@endsection
