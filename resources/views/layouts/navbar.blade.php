<div class="container">
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Navbar</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownEstudiantes" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Estudiantes
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownEstudiantes">
          <a class="dropdown-item" href="{{ route('estudiante.create') }}">Registro</a>
          <a class="dropdown-item" href="{{ route('estudiante.index') }}">Listado</a>
          <div class="dropdown-divider"></div>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownEmpleados" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Empleados
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownEmpleados">
          <a class="dropdown-item" href="{{ route('empleado.create') }}">Registro</a>
          <a class="dropdown-item" href="{{ route('empleado.index') }}">Listado</a>

          <div class="dropdown-divider"></div>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownLibros" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Libros
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownLibros">
          <a class="dropdown-item" href="{{ route('libro.create') }}">Registro</a>
          <a class="dropdown-item" href="{{ route('libro.index') }}">Listado</a>

          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Busqueda de libros</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPrestamo" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Prestamo
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownPrestamo">
          <a class="dropdown-item" href="{{ route('prestamo.create') }}">Registro</a>
          <a class="dropdown-item" href="{{ route('prestamo.index') }}">Listado</a>

          <div class="dropdown-divider"></div>
        </div>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>
</div>