@extends('master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @forelse ($empleados as $empleado)
        @if ($loop->first)
        <div class="col-md-8 text-center">
            <h1>Empleados Disponibles</h1>
        </div>
        @endif
        
        <div class="col-md-8 mt-2">
            <div class="card">
                <div class="card-header"> <a class="btn" href="{{ route('empleado.show', $empleado) }}"> {{$empleado->title}} </a></div>
                <div class="card-body">
                <div>
                <p>Nombre: {{$empleado->nombre}}</p>
                    <p>Apellido: {{$empleado->apellido}}</p>
                    <p>Cedula: {{$empleado->cedula}}</p>
                    <p>Telefono: {{$empleado->telefono}}</p>
                    <p>Correo Electronico: {{$empleado->correo}}</p>
                </div>
                </div>
            </div>
        </div>

         <div class=" d-inline-flex ">
            <a href="{{route('empleado.edit', $empleado)}}" class="btn btn-outline-success btn-md mr-2">Editar</a>
                <form action="{{route('empleado.destroy', $empleado)}}" method="POST">
                    @method('DELETE')
                    @csrf
                    <button type="submit" href="{{route('empleado.edit', $empleado)}}" class="btn btn-danger btn-md">Borrar</button>
                    </form>
        </div>


        @empty
        <div class="col-md-8 text-center">
            <h1>Aun no hay paquetes disponibles!</h1>
        </div>
        @endforelse
    </div>
</div>
@endsection
