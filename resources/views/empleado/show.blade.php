@extends('master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
    
        <div class="col-md-8 text-center">
            <h1>Empleados Disponibles</h1>
        </div>
              
        <div class="col-md-8 mt-2">
            <div class="card">
                <div class="card-body">
                <div>
                    <p>Nombre: {{$empleado->nombre}}</p>
                    <p>Apellido: {{$empleado->apellido}}</p>
                    <p>Cedula: {{$empleado->cedula}}</p>
                    <p>Telefono: {{$empleado->telefono}}</p>
                    <p>Correo Electronico: {{$empleado->correo}}</p>
                </div>
                </div>
            </div>
        </div>
      
    </div>
</div>
@endsection
