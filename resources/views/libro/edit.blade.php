@extends('master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Registrar Estudiante</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('libro.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="nombre" class="col-md-4 col-form-label text-md-right">Nombre</label>

                            <div class="col-md-6">
                                <input id="nombre" type="text" class="form-control{{ $errors->has('nombre') ? ' is-invalid' : '' }}" name="nombre" value="{{ libro->nombre }}" required autofocus>

                                @if ($errors->has('nombre'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nombre') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="editorial" class="col-md-4 col-form-label text-md-right">Editorial</label>

                            <div class="col-md-6">
                                <input id="editorial" type="text" class="form-control{{ $errors->has('editorial') ? ' is-invalid' : '' }}" name="editorial" value="{{ libro->editorial }}" required autofocus>

                                @if ($errors->has('editorial'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('editorial') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="anio" class="col-md-4 col-form-label text-md-right">Año</label>

                            <div class="col-md-6">
                                <input id="anio" type="text" class="form-control{{ $errors->has('anio') ? ' is-invalid' : '' }}" name="anio" value="{{ libro->anio }}" required autofocus>

                                @if ($errors->has('anio'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('anio') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="ubicacion" class="col-md-4 col-form-label text-md-right">Ubicaciòn en la biblioteca</label>

                            <div class="col-md-6">
                                <input id="ubicacion" type="text" class="form-control{{ $errors->has('ubicacion') ? ' is-invalid' : '' }}" name="ubicacion" value="{{ libro->ubicacion }}" required autofocus>

                                @if ($errors->has('ubicacion'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('ubicacion') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="autor" class="col-md-4 col-form-label text-md-right">Autor</label>

                            <div class="col-md-6">
                                <input id="autor" type="text" class="form-control{{ $errors->has('autor') ? ' is-invalid' : '' }}" name="autor" value="{{ libro->autor }}" required autofocus>

                                @if ($errors->has('autor'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('autor') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="tipo" class="col-md-4 col-form-label text-md-right">Tipo(Normal-Reserva)</label>

                            <div class="col-md-6">
                                <input id="tipo" type="text" class="form-control{{ $errors->has('tipo') ? ' is-invalid' : '' }}" name="tipo" value="{{ libro->tipo }}" required autofocus>

                                @if ($errors->has('tipo'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('tipo') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                          <div class="form-group row">
                            <label for="area" class="col-md-4 col-form-label text-md-right">Area de conocimiento</label>

                            <div class="col-md-6">
                                <input id="area" type="text" class="form-control{{ $errors->has('area') ? ' is-invalid' : '' }}" name="area" value="{{ libro->area }}" required autofocus>

                                @if ($errors->has('area'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('area') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                          <div class="form-group row">
                            <label for="dias" class="col-md-4 col-form-label text-md-right">Dias de prestamo</label>

                            <div class="col-md-6">
                                <input id="dias" type="text" class="form-control{{ $errors->has('dias') ? ' is-invalid' : '' }}" name="dias" value="{{ libro->dias }}" required autofocus>

                                @if ($errors->has('dias'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('dias') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Registrar
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
