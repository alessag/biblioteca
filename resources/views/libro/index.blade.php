@extends('master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @forelse ($libros as $libro)
        @if ($loop->first)
        <div class="col-md-8 text-center">
            <h1>libros Actuales</h1>
        </div>
        @endif
        
        <div class="col-md-8 mt-2">
            <div class="card">
                <div class="card-header"> <a class="btn" href="{{ route('libro.show', $libro) }}"> {{$libro->title}} </a></div>
                <div class="card-body">
                <div>
                    <p>Nombre: {{$libro->nombre}}</p>
                    <p>Editorial: {{$libro->editorial}}</p>
                    <p>Año: {{$libro->anio}}</p>
                    <p>Ubicación en la biblioteca: {{$libro->ubicacion}}</p>
                    <p>Autor: {{$libro->autor}}</p>
                    <p>Tipo: {{$libro->tipo}}</p>
                    <p>Area de conocmiento: {{$libro->area}}</p>
                    <p>Dias prestamo: {{$libro->dias-prestamo}}</p>
                </div>
                </div>
            </div>
        </div>

         <div class=" d-inline-flex ">
            <a href="{{route('libro.edit', $libro)}}" class="btn btn-outline-success btn-md mr-2">Editar</a>
                <form action="{{route('libro.destroy', $libro)}}" method="POST">
                    @method('DELETE')
                    @csrf
                    <button type="submit" href="{{route('libro.edit', $libro)}}" class="btn btn-danger btn-md">Borrar</button>
                    </form>
        </div>


        @empty
        <div class="col-md-8 text-center">
            <h1>Aun no hay libros</h1>
        </div>
        @endforelse

       

    </div>
</div>
@endsection
