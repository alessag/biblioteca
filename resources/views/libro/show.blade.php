@extends('master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
    
        <div class="col-md-8 text-center">
            <h1>Libros Disponibles</h1>
        </div>
              
        <div class="col-md-8 mt-2">
            <div class="card">
                <div class="card-body">
                <div>
                    <p>Nombre: {{$libro->nombre}}</p>
                    <p>Editorial: {{$libro->editorial}}</p>
                    <p>Año: {{$libro->anio}}</p>
                    <p>Ubicación en la biblioteca: {{$libro->ubicacion}}</p>
                    <p>Autor: {{$libro->autor}}</p>
                    <p>Tipo: {{$libro->tipo}}</p>
                    <p>Area de conocmiento: {{$libro->area}}</p>
                    <p>Dias prestamo: {{$libro->dias-prestamo}}</p>
                </div>
                </div>
            </div>
        </div>
      
    </div>
</div>
@endsection
