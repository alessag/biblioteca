<?php

namespace App\Http\Controllers;

use App\Estudiante;
use Illuminate\Http\Request;

class EstudianteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estudiantes = Estudiante::all();
        return view('estudiante.index', compact('estudiantes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('estudiante.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'nombre' => 'required|string|min:5|max:50',
            'apellido' => 'required|string|min:5|max:150',
            'cedula' => 'required|string|min:8',
            'telefono' => 'required|string|min:11',
            'correo' => 'required|string|min:10|max:150',
            'carrera' => 'required|string|min:10|max:150',
        ]);

        $estudiante = Estudiante::create($validated);
        return redirect()->route('estudiante.show' , $estudiante)->withSuccess('Servicio creado con exito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Estudiante  $estudiante
     * @return \Illuminate\Http\Response
     */
    public function show(Estudiante $estudiante)
    {
        return view('estudiante.show',compact('estudiante'));   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Estudiante  $estudiante
     * @return \Illuminate\Http\Response
     */
    public function edit(Estudiante $estudiante)
    {
        return view('estudiante.edit', compact('estudiante'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Estudiante  $estudiante
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Estudiante $estudiante)
    {
        $validated = $request->validate([
            'nombre' => 'required|string|min:5|max:50',
            'apellido' => 'required|string|min:5|max:150',
            'cedula' => 'required|string|min:8',
            'telefono' => 'required|string|min:11',
            'correo' => 'required|string|min:10|max:150',
            'carrera' => 'required|string|min:10|max:150',
        ]);

        $estudiante->fill($validated);
        $estudiante->save();
        return redirect()->route('estudiante.show' , $estudiante)->withSuccess('Estudiante agregado con exito ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Estudiante  $estudiante
     * @return \Illuminate\Http\Response
     */
    public function destroy(Estudiante $estudiante)
    {
        $estudiante->delete();
        return redirect()->route('estudiante.index')->withSuccess('Borrado con exito');
    }
}

