<?php

namespace App\Http\Controllers;

use App\Libro;
use Illuminate\Http\Request;

class LibroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $libros = libro::all();
        return view('libro.index', compact('libros'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('libro.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        dd ($request->all ());
        $validated = $request->validate([
            'nombre' => 'required|string|min:10|max:50',
            'editorial' => 'required|string|min:10|max:150',
            'anio' => 'required|date',
            'ubicacion' => 'required|string|min:10|max:150',
            'autor' => 'required|string|min:10|max:150',
            'tipo' => 'required|string|min:10|max:150',
            'area' => 'required|string|min:1',
            'dias-prestamo' => 'required|numeric|min:1',

        ]);

        $libro = libro::create($validated);
        return redirect()->route('libro.show' , $libro)->withSuccess('Servicio creado con exito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Libro  $libro
     * @return \Illuminate\Http\Response
     */
    public function show(Libro $libro)
    {
        return view('libro.show',compact('libro'));        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Libro  $libro
     * @return \Illuminate\Http\Response
     */
    public function edit(Libro $libro)
    {
        return view('libro.edit', compact('libro'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Libro  $libro
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Libro $libro)
    {
        $validated = $request->validate([
            'nombre' => 'required|string|min:10|max:50',
            'editorial' => 'required|string|min:10|max:150',
            'anio' => 'required|date|',
            'ubicacion' => 'required|string|min:10|max:150',
            'autor' => 'required|string|min:10|max:150',
            'tipo' => 'required|string|min:10|max:150',
            'area' => 'required|string|min:1',
            'dias-prestamo' => 'required|numeric|min:1',
        ]);

        $libro->fill($validated);
        $libro->save();
        return redirect()->route('libro.show' , $libro)->withSuccess('Servicio editado con exito!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Libro  $libro
     * @return \Illuminate\Http\Response
     */
    public function destroy(Libro $libro)
    {
        $libro->delete();
        return redirect()->route('libro.index')->withSuccess('Servicio borrado con exito');
    }
}
