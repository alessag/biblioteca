<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Empleado;

class Empleado extends Model
{
     /**
     * @var array
     */
    protected $guarded =[];

    public function prestamos()
    {
        return $this->hasMany(Prestamo::class);
    }
}
